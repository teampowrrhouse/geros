package com.example.powerhouse.geros;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class HomeActivity extends AppCompatActivity
        implements GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMapLongClickListener,
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth firebaseAuth;

    //view objects
    private TextView textViewUserEmail;
    private GoogleMap mMap;
    private boolean mPermissionDenied = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private Marker destiMarker;
    LatLng destination;
    Location origin;
    ImageView btnstop,btnpause,btnrun;
    TextView textSource, textDestination;
    private FusedLocationProviderClient mFusedLocationClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "MainActivity";
    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private String userID;
    GoogleApiClient mGoogleApiClient;
    Button process, generatePath;
    DatabaseReference mDatabase;
    Context context = this;
    CSP csp = new CSP();
    Boolean clicked = false;
    TextView txtdistance;

    @Override
    protected void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();


    }


    HashMap<String, VerticesDB> vertices = new HashMap<>();
    List<PathsDB> paths2 = new ArrayList<>();


    //Adding  option to firebase app. This can be seen inside googe-services.json file
    FirebaseOptions options = new FirebaseOptions.Builder()
            .setApiKey("AIzaSyAOjY3SY7KwLb9fzY2YX0xzVRVLG7gECyw")  //apikey in the .json file
            .setApplicationId("1:1019572731798:android:aa3312d4e15d4ed8") //mobilesdk_app_id in .json file
            .setDatabaseUrl("https://mapsdb-3fd78.firebaseio.com/") //url of the database in console.firebase.google.com
            .build();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setTitle("Geros");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        final FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();
        View header = navigationView.getHeaderView(0);
        TextView email = (TextView) header.findViewById(R.id.etmail);
        email.setText(user.getEmail());
        process = (Button)findViewById(R.id.btnProcess);
        generatePath = (Button) findViewById(R.id.btnPath);
        txtdistance=(TextView)findViewById(R.id.txtdistance);
        textSource = (TextView) findViewById(R.id.actvSouce);
        textDestination = (TextView) findViewById(R.id.actvDestination);
        btnpause = (ImageView) findViewById(R.id.btnpause);
        btnstop = (ImageView) findViewById(R.id.btnstop);
        btnrun = (ImageView) findViewById(R.id.btnrun);

        //if the user is not logged in
        //that means current user will return null
        mAuth = FirebaseAuth.getInstance();
        mAuthListener=new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()== null){
                    startActivity((new Intent(HomeActivity.this,LoginActivity.class)));
                }
            }
        };


        //check if data exists in firebase
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("users").child(userID).child("weight").exists()){
//                    Toast.makeText(HomeActivity.this, "Welcome!" , Toast.LENGTH_SHORT).show();
                }
                else{
                    try{
                        FragmentManager manager = getSupportFragmentManager();
                        ProfileFragment profileFragment = new ProfileFragment();
                        manager.beginTransaction()
                                .replace(R.id.mainLayout, profileFragment, profileFragment.getTag())
                                .commit();
                        process.setEnabled(true);
                        process.setAlpha(0f);

                    }
                    catch(NullPointerException e) {
                        e.printStackTrace();
                    }}}
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //....

        //This is for the myLocation button
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
        //....

        //This will get the location of the current location.
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        //This is for the Firebase Database.
        try {
            FirebaseApp secondApp = FirebaseApp.initializeApp(HomeActivity.this, options, "second app");
            mDatabase = FirebaseDatabase.getInstance(secondApp).getReference(); //get instance of the firebase database
            }
        catch (IllegalStateException e)
        {
            mDatabase = FirebaseDatabase.getInstance(FirebaseApp.getInstance("second app")).getReference(); //get instance of the firebase database
        }
        //...

        vertices = getVertexDB(); //will call call the method which consist of getting the data from firebase.
        getPathDB(); //will call the method which consists of getting the data from firebase.



        btnrun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnrun.setEnabled(false);
                btnrun.setAlpha(0f);
                btnpause.setAlpha(1f);
                btnstop.setAlpha(1f);
            }
        });

        generatePath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("HomeActivity", "button clicked");
                List<Location> zpath = csp.pathGeneration();
                Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_LONG).show();
                Log.e("Location", "ZPATHS = "+ zpath.size());
                generatePath.setEnabled(false);
                process.setEnabled(false);
                generatePath.setAlpha(0f);
                btnrun.setAlpha(1f);


                for(int x=0; x<zpath.size()-1; x++)
                {
                    PolylineOptions polylineOptions = new PolylineOptions()
                            .add(new LatLng(zpath.get(x).getLatitude(), zpath.get(x).getLongitude()),
                            new LatLng(zpath.get(x+1).getLatitude(), zpath.get(x+1).getLongitude()));
                    mMap.addPolyline(polylineOptions);
                }

//                Toast.makeText(getApplicationContext(), String.valueOf(csp.getBlah()), Toast.LENGTH_SHORT).show();
//                List<String> keys = new ArrayList<String>(zpath.keySet());
//                List<LatLng> forPolyLine = new ArrayList<LatLng>();
//                PolylineOptions polylineOptions = new PolylineOptions();
//                Log.e("Location", "ZPATHS = "+ keys.size());
//                for(int x=0; x<keys.size()-1;x++)
//                {
//                    polylineOptions.add(new LatLng(zpath.get(keys.get(x)).getVertexLat(),
//                                    zpath.get(keys.get(x)).getVertexLong()),
//                            new LatLng(zpath.get(keys.get(x+1)).getVertexLat(),
//                                    zpath.get(keys.get(x+1)).getVertexLong()));
//                    mMap.addPolyline(polylineOptions);
//                }


            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        {

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
//    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager manager = getSupportFragmentManager();


        if (id == R.id.start_run) {
            startActivity(new Intent(this, HomeActivity.class));

        }
        else if (id == R.id.nav_profile) {
            // Handle the camera action

            ProfileFragment profileFragment = new ProfileFragment();
            manager.beginTransaction()
                    .replace(R.id.mainLayout, profileFragment, profileFragment.getTag())
                    .commit();
            generatePath.setAlpha(0f);
            process.setAlpha(0f);


        } else if (id == R.id.nav_Logout) {
            mAuth.signOut();
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            startActivity((new Intent(HomeActivity.this, LoginActivity.class)));
                        }
                    });

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);
        //enableMyLocation();
        mMap.setOnMapLongClickListener(this);
        forOpening();

        final LatLng smManila = new LatLng(14.589807427154074, 120.98408963531256);
        final LatLng grandstand = new LatLng(14.581924737903073, 120.98129108548166);
//        final LatLng manila = new LatLng(14.583280396246812, 120.98053570836782);
        final LatLng manila = new LatLng(14.585274607068108,120.97933642566203);
        mMap.addMarker(new MarkerOptions().position(smManila));
        mMap.addMarker(new MarkerOptions().position(grandstand));
        mMap.setOnMarkerClickListener(this);

        process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LatLngBounds.Builder builder = new LatLngBounds.Builder().include(manila).include(grandstand);
                CameraPosition bla = new CameraPosition.Builder()
                        .target(manila)
                        .zoom(15)
                        .bearing(-45)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(bla));
                Toast.makeText(getApplicationContext(), "Choose Starting destination", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }

    }

    public void forOpening() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
            return;
        }
        else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null)
                        {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                                    .zoom(17)                   // Sets the zoom
                                    .bearing(0)                // Sets the orientation of the camera to east
                                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                                    .build();                   // Creates a CameraPosition from the builder
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            textSource.setText(String.valueOf(forAddress(location.getLongitude(), location.getLatitude())));
                            origin = location;
                        }
                    }
                });



    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "Current Location", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        //forAddress();
        return false;
    }

    public String forAddress(double sourceLong, double sourceLat)
    {
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLatitude());
        Geocoder geocoder = new Geocoder(this);
        List<Address> addressList = null;
        try {
            addressList = geocoder.getFromLocation(sourceLat, sourceLong, 1);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return addressList.get(0).getAddressLine(0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments(){
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onMapLongClick (LatLng point)
    {
        destiMarker = mMap.addMarker(new MarkerOptions()
                .position(point)
        );
        destination = point;
        textDestination.setText(String.valueOf(forAddress(point.longitude, point.latitude)));
        forRoutes(destination, origin);
    }

    public void forRoutes(LatLng desti, Location ori)
    {
        mMap.addPolyline(new PolylineOptions().add(
                new LatLng(ori.getLatitude(), ori.getLongitude()),
                desti
        ));



        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker destiMarker) {
                return true;
            }
        });

    }

    public HashMap<String, VerticesDB> getVertexDB()
    {
        final HashMap<String, VerticesDB> vertexList = new HashMap<>();
        final List <PathsDB> paths2 = csp.getPath2();

        mDatabase.child("vertex").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren())
                {
                    VerticesDB verticess = new VerticesDB();
                    verticess.setVertexLat(ds.child("").getValue(VerticesDB.class).getVertexLat());
                    verticess.setVertexLong(ds.child("").getValue(VerticesDB.class).getVertexLong());
                    verticess.setvName(ds.child("").getValue(VerticesDB.class).getvName());
                    verticess.setvID(ds.child("").getValue(VerticesDB.class).getvID());

                    vertexList.put(verticess.getvID(), verticess);

                }

//                HashMap<String, VerticesDB> temp = new HashMap<>();
//                for (int i = 0; i < vertexList.size(); i++) {
//                    temp.put(vertexList.get(i).getvID(), vertexList.get(i));
//                }

               // Log.e("checker", "reachnodes = "+ vertices.get("14,581924737903073x120,98129108548166").getReachableNodesID().get(0));
                csp.setVertices(vertexList);
                Toast.makeText(getApplicationContext(), String.valueOf(vertexList.size()), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return vertexList;
    }

    public void getPathDB()
    {
        final HashMap<String, PathsDB> pathsList = new HashMap<>();
        mDatabase.child("path").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :dataSnapshot.getChildren())
                {
                    PathsDB paths = new PathsDB();
                    paths.setStartingNode(ds.child("").getValue(PathsDB.class).getStartingNode());
                    paths.setFinalNode(ds.child("").getValue(PathsDB.class).getFinalNode());
                    paths.setPathID(ds.child("").getValue(PathsDB.class).getPathID());

                    pathsList.put(paths.getPathID(), paths);
                    paths2.add(paths);
                }
                Log.e("Checker", "csp Paths List = " + paths2.size());
                csp.setPath2(paths2);
                paths2 = csp.getPath2();
                reachNodes();
                csp.setPaths(pathsList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (!clicked)
        {
            double lat = marker.getPosition().latitude;
            double lng = marker.getPosition().longitude;
            String key = lat + "x" + lng;

            char[] forVID = key.toCharArray();
            for (int x=0; x<forVID.length; x++)
                if (forVID[x] == '.')
                    forVID[x] = ',';
            key = String.valueOf(forVID);
            VerticesDB startNode = null;

            startNode = vertices.get(key);

            if (startNode == null) return true;

            csp.setCurrentLocation(startNode);
            clicked = true;
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            process.setText("CHOOSE DESTINATION");
            process.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "NOW CHOOSE DESTINATION", Toast.LENGTH_LONG).show();
                }
            });
            return true;
        }

        else
        {
            double lat = marker.getPosition().latitude;
            double lng = marker.getPosition().longitude;
            String key = lat + "x" + lng;

            char[] forVID = key.toCharArray();
            for (int x=0; x<forVID.length; x++)
                if (forVID[x] == '.')
                    forVID[x] = ',';
            key = String.valueOf(forVID);
            VerticesDB endNode = null;

            endNode = vertices.get(key);

            csp.setDestination(endNode);
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            process.setText("ADD DISTANCE");
            process.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder ab = new AlertDialog.Builder(HomeActivity.this);
                    ab.setTitle("Enter Kilometer(km): 1-10");
                    final EditText txt = new EditText(HomeActivity.this);
                    txt.setRawInputType(Configuration.KEYBOARD_12KEY);
                    txt.setInputType(InputType.TYPE_CLASS_NUMBER);
                    ab.setView(txt);

                    ab.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Double value = Double.parseDouble(txt.getText().toString());
                            csp.setDistance(value*1000);
//                            txtdistance.setText(csp.getDistance());
                            generatePath.setAlpha(1f);
                            process.setAlpha(0f);

                        }
                    });
                    ab.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            txtdistance.setText("No Number entered");

                        }
                    });
                    AlertDialog a=ab.create();
                    a.show();

                }
            });
            return true;
        }
    }

    public void reachNodes()
    {
        List<String> keys = new ArrayList<>(vertices.keySet());

        for(int y=0; y<keys.size();y++)
        {
            List<String> reachableNodes = new ArrayList<>();
            String id = vertices.get(keys.get(y)).getvID();
            Log.e("Checker",  "VerticesPaths List = " + paths2.size());
            for (int x = 0; x < paths2.size(); x++) {
                PathsDB path = paths2.get(x);

                if (id.equals(path.getStartingNode())) {
                    id = path.getFinalNode();
                    if (id.equals(vertices.get(id).getvID())) {
                        reachableNodes.add(vertices.get(id).getvID());
                        break;
                    }

                }
                else {
                    id = path.getStartingNode();
                    if (id.equals(vertices.get(id).getvID())) {
                        reachableNodes.add(vertices.get(id).getvID());
                        break;
                    }
                }
            }
            vertices.get(keys.get(y)).setReachableNodesID(reachableNodes);
            Log.e("Checker", "while Paths List = " +vertices.get(keys.get(y)).getReachableNodesID().size());
        }


    }

}
