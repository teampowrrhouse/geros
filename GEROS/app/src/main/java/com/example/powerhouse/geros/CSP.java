package com.example.powerhouse.geros;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by CJ on 8/23/2017.
 */

public class CSP {

    VerticesDB startNode = null;
    VerticesDB endNode = null;
    HashMap<String, VerticesDB> vertexDB = new HashMap<>();
    HashMap<String, PathsDB> pathsDB = new HashMap<>();
    double sourceLat, sourceLong, destiLat, destiLong, minDistance, maxDistance;
    List <Location> zPath = new ArrayList<>();
    List <String> forID = new ArrayList<>();
    HashMap <String, VerticesDB> zPath1 = new HashMap<>();
    int counter;
    List<PathsDB> path2 = new ArrayList<>();

    public CSP()
    {

    }


    public void setVertices (HashMap<String, VerticesDB> vertexDB)
    {
        this.vertexDB = vertexDB;
        Log.e("verticeCount", vertexDB.size() + "");
    }

    public void setPaths (HashMap<String, PathsDB> pathsDB)
    {
        this.pathsDB = pathsDB;
    }

    public void setCurrentLocation (VerticesDB startNode)
    {
        this.startNode = startNode;
    }

    public void setDestination(VerticesDB endNode) {
        this.endNode = endNode;
    }

    public void setPath2 (List<PathsDB> path2) {
        this.path2 = path2;
        Log.e("Checker", "sa CSP Paths List = " + path2.size());
    }
    public List<PathsDB> getPath2 () {return  path2;}


    public void setDistance(double distance)
    {
        this.minDistance =distance;
        maxDistance = minDistance + 2000;
    }

    public String getDistance(){ return String.valueOf(minDistance) + "&&" + String.valueOf(maxDistance);}


    public List<Location> pathGeneration()
    {
        HashMap<String, VerticesDB> paths = new HashMap<>();
        paths.put(startNode.getvID(), startNode);
        generatePath(startNode.getvID() ,paths);
        return zPath;
    }

    //generates the path
    public void generatePath(String vID ,HashMap<String, VerticesDB> paths) {
    }
}
