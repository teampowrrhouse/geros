package com.example.powerhouse.geros;

/**
 * Created by nicko on 8/7/2017.
 */

public class UserInformation {
    private String fname;
    private String lname;
    private String height;
    private String weight;

    public UserInformation(){

    }

    public UserInformation(String fname, String lname, String weight,String height) {
        this.fname = fname;
        this.lname = lname;
        this.weight = weight;
        this.height = height;


    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) { this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) { this.height = height;
    }



}