package com.example.powerhouse.geros;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;






/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileDatabase";
    private FirebaseDatabase mFirebaseDatabase;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private TextView etfname, etlname, etweight, etheight, etemail;
    private Button buttonsave;
    private String userID;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Profile");

// Get Firebase auth instance
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        //if the user is not logged in
        //that means current user will return null

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Toast.makeText(getActivity(), "Sign Out", Toast.LENGTH_SHORT).show();

                }
                // ...
            }
        };
        myRef.addValueEventListener(new ValueEventListener() {

            ProgressDialog dialog = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                showData(dataSnapshot);
                dialog.dismiss();
                Log.d(TAG, "onDataChange: Added information to database: \n" +
                        dataSnapshot.getValue());

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG,"Please Check your Internet connection", error.toException());
            }
        });


        View mview = inflater.inflate(R.layout.fragment_profile, container, false);
        etfname = (EditText) mview.findViewById(R.id.etfname);
        etlname = (EditText) mview.findViewById(R.id.etlname);
        etheight = (EditText) mview.findViewById(R.id.etheight);
        etweight = (EditText) mview.findViewById(R.id.etweight);
        etemail = (TextView) mview.findViewById(R.id.etemail);
        buttonsave = (Button) mview.findViewById(R.id.btnsave);

        //retun home
        mview.setFocusableInTouchMode(true);
        mview.requestFocus();
        mview.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                        return true;
                    }
                }
                return false;
            }
        });


        //displaying logged in user name
        etemail.setText(user.getEmail());

        buttonsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View s) {
                saveUserInformation();
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
            }

        });
        return mview;


    }

    private void saveUserInformation() {
        Log.d(TAG, "onClick: Submit pressed.");
        String fname = etfname.getText().toString().trim();
        String lname = etlname.getText().toString().trim();
        String weight = etweight.getText().toString().trim();
        String height = etheight.getText().toString().trim();

        if(!fname.equals("") && !lname.equals("") && !weight.equals("")&& !height.equals("")&& !weight.equals("")){
            UserInformation userInformation = new UserInformation(fname,lname,weight,height);
            myRef.child("users").child(userID).setValue(userInformation);
            Toast.makeText(getActivity(), "New Information has been saved.", Toast.LENGTH_SHORT).show();


        }else{
            Toast.makeText(getActivity(), "Fill out all the fields", Toast.LENGTH_SHORT).show();
        }
    }



    private void showData(DataSnapshot dataSnapshot) {
        try {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                UserInformation uInfo = new UserInformation();
                uInfo.setFname(ds.child(userID).getValue(UserInformation.class).getFname()); //set the fname
                uInfo.setLname(ds.child(userID).getValue(UserInformation.class).getLname()); //set the lname
                uInfo.setHeight(ds.child(userID).getValue(UserInformation.class).getHeight()); //set the height
                uInfo.setWeight(ds.child(userID).getValue(UserInformation.class).getWeight()); //set the weight

                Log.d(TAG, "showData: First Name: " + uInfo.getFname());
                Log.d(TAG, "showData: Last Name: " + uInfo.getLname());
                Log.d(TAG, "showData: Height: " + uInfo.getHeight());
                Log.d(TAG, "showData: Weight: " + uInfo.getWeight());

                etfname.setText(uInfo.getFname());
                etlname.setText(uInfo.getLname());
                etheight.setText(uInfo.getHeight());
                etweight.setText(uInfo.getWeight());

            }
        }

        catch(NullPointerException e){
            e.printStackTrace();
            Toast.makeText(getActivity(), "Fill up the Information", Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public void onStart() {

        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }



    }}